<?php

$breaking_news = select_all_title();


?>
<div class="header_top_bar_container clearfix">
    <div class="header_top_bar">
        <form class="search" action="search.html" method="get">
            <input type="text" name="s" placeholder="Search..." value="Search..." class="search_input hint">
            <input type="submit" class="search_submit" value="">
        </form>
        <!--<ul class="social_icons dark clearfix">
        <ul class="social_icons colors clearfix">-->
        <ul class="social_icons clearfix">
            <li>
                <a target="_blank" href="https://www.facebook.com/footballfansbd.official/" class="social_icon facebook" title="facebook">
                    &nbsp;
                </a>
            </li>
            <li>
                <a target="_blank" href="https://twitter.com/inverse_shakil" class="social_icon twitter" title="twitter">
                    &nbsp;
                </a>
            </li>
            <li>
                <a href="mailto:shakil@dboxb.com" class="social_icon mail" title="mail">
                    &nbsp;
                </a>
            </li>
            
        </ul>
        <div class="latest_news_scrolling_list_container">
            <ul>
                <li class="category">LATEST</li>
                <li class="left"><a href="#"></a></li>
                <li class="right"><a href="#"></a></li>
                <li class="posts">

                    <ul class="latest_news_scrolling_list">
                       <?php  while ($latest_update = mysqli_fetch_assoc($breaking_news)) { ?>
                            <li>
                                <a href="post_details.php?post_id=<?php echo $latest_update['post_id'] ?>" title="<?php echo $latest_update['post_title'] ?>"><?php echo $latest_update['post_title'] ?></a>
                            </li>
                        <?php   } ?>
                    </ul>

                </li>

            </ul>
        </div>
    </div>
</div>