
    <div class="footer clearfix">
        <div class="row" >
            <h4 class="box_header page_margin_top center" align="center">Get In Touch With Us</h4>
            <ul class="social_icons dark page_margin_top clearfix" style="margin-left: 400px;">
                    <li>
                        <a target="_blank" title="" href="https://www.facebook.com/footballfansbd.official/" class="social_icon facebook">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a target="_blank" title="" href="https://twitter.com/inverse_shakil" class="social_icon twitter">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a title="" href="mailto:shakil@dboxb.com" class="social_icon mail">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a title="" href="#" class="social_icon skype">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a title="" href="shakil@dboxb.com" class="social_icon envato">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a title="" href="https://www.instagram.com/inverse.shakil/" class="social_icon instagram">
                            &nbsp;
                        </a>
                    </li>
                    <li>
                        <a title="" href="#" class="social_icon pinterest">
                            &nbsp;
                        </a>
                    </li>
                </ul>
        </div>
        <div class="row page_margin_top_section">
            <div class="column column_3_4">
                <ul class="footer_menu">
                    <li>
                        <h4><a href="fixture.php" title="Fixtures">Fixtures</a></h4>
                    </li>
                    <li>
                        <h4><a href="point_table.php" title="Point Tables">Point Tables</a></h4>
                    </li>
                    <li>
                        <h4><a href="articles.php" title="Featured">Featured</a></h4>
                    </li>
                    <li>
                        <h4><a href="legends_columnn.php" title="Legends Column">Legends Column</a></h4>
                    </li>
                    <li>
                        <h4><a href="epl.php" title="English Premier League">English Premier League</a></h4>
                    </li>
                </ul>
            </div>
            <div class="column column_1_4">
                <a class="scroll_top" href="#top" title="Scroll to top">Top</a>
            </div>
        </div>
        <div class="row copyright_row">
            <div class="column column_2_3">
                © Copyright <a href="https://www.facebook.com/inverse.shakil" title="Shakil" target="_blank">Shakil</a> - Administrator of Football Fans Bangladesh 
            </div>
            <div class="column column_1_3">
                <ul class="footer_menu">
                    <li>
                        <h6><a href="about_us.php" title="About">About</a></h6>
                    </li>
                    <li>
                        <h6><a href="author_list.php" title="Authors">Authors</a></h6>
                    </li>
                </ul>
            </div>
        </div>
    </div>
