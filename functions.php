<?php

//----Home Page Slider----//

function select_featured_post_for_slider() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='8' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 1 ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_epl_post_for_slider() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='1' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_laliga_post_for_slider() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='2' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_bangladesh_post_for_slider() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='5' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//----Content Post Details Page----//

function select_post_info_by_id($post_id) {
    require './admin/db_connect.php';
    $sql = "SELECT p.*, a.author_name, a.designation, a.author_image, c.category_name, t.tag_name FROM tbl_posts as p, tbl_author as a, tbl_category as c, tbl_tag as t WHERE p.author_id=a.author_id AND p.category_id=c.category_id AND p.tag_id=t.tag_id AND p.post_id='$post_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_category_post_by_id() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE publication_status =1 AND deletion_status=1 ORDER BY RAND() LIMIT 6";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_published_cat() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_category WHERE publication_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_published_tag() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_tag";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//--Content Body For EPL-La Liga-Bundes Liga-Champions League--//

function select_epl_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='1' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_laliga_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='2' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_featured_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='8' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_ucl_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='3' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_worldfootball_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='6' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_bangladesh_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='5' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_legends_column_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='9' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_video_archive_post_for_index() {
    require 'admin/db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='7' AND publication_status =1 AND deletion_status=1 ORDER BY post_id DESC LIMIT 4";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_latest_post_index() {
    require 'db_connect.php';
    $sql = "SELECT p.*, c.category_name FROM tbl_posts as p, tbl_category as c WHERE p.category_id=c.category_id ORDER BY post_id DESC LIMIT 6";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---author info---//

function select_authors_info() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_author_index() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function author_info_by_id($author_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE author_id='$author_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_post_by_id($author_id) {
    require 'db_connect.php';
    $sql = "SELECT p.*, c.category_name FROM tbl_posts as p, tbl_category as c WHERE p.category_id=c.category_id AND author_id='$author_id' ORDER BY post_id DESC LIMIT 4 ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---EPL PAGE Content---//

function select_all_epl_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='1' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---LALIGA PAGE Content---//

function select_all_laliga_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='2' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---BUNDESLIGA PAGE Content---//

function select_all_bundesliga_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='4' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---UCL PAGE Content---//

function select_all_ucl_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='3' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---Bangladesh PAGE Content---//

function select_all_bd_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='5' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---Bangladesh PAGE Content---//

function select_all_featured_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='8' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//---Bangladesh PAGE Content---//

function select_all_legends_column_post() {
    require './db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE category_id='9' AND publication_status=1 ORDER BY post_id DESC";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_title(){
    require './db_connect.php';
    $sql= "SELECT post_title FROM tbl_posts WHERE publication_status=1 ORDER BY post_id DESC LIMIT 6";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}