<?php
$query_result = select_featured_post_for_slider();
$query_result_epl = select_epl_post_for_slider();
$query_result_laliga = select_laliga_post_for_slider();
$query_result_bangladesh = select_bangladesh_post_for_slider();

$epl_index = select_epl_post_for_index();
$laliga_index= select_laliga_post_for_index();
$featured_index= select_featured_post_for_index();
$ucl_index= select_ucl_post_for_index();
$worldfootball_index= select_worldfootball_post_for_index();
$bangladesh_index= select_bangladesh_post_for_index();
$legends_column_index= select_legends_column_post_for_index();

$latest_post_index= select_all_latest_post_index();
$author_list_index= select_all_author_index();

$published_category= select_all_published_cat();
$tag_result= select_all_published_tag();
?>

<ul class="slider">
    <?php while ($feature_post_info = mysqli_fetch_assoc($query_result)) { ?>
        <li class="slide">
            <img src='admin/<?php echo $feature_post_info['post_image']; ?>' alt='img'>
            <div class="slider_content_box">
                <ul class="post_details simple">
                    <li class="category"><a href="" title="Features">Features</a></li>
                    <li class="date">
                        10:11 PM, Feb 02
                    </li>
                </ul>
                <h2><a href="post_details.php?post_id=<?php echo $feature_post_info['post_id']; ?>" title="<?php echo $feature_post_info['post_title']; ?>"><?php echo $feature_post_info['post_title']; ?></a></h2>

            </div>
        </li>
    <?php } ?>
    <?php while ($epl_post_info = mysqli_fetch_assoc($query_result_epl)) { ?>
        <li class="slide">
            <img src='admin/<?php echo $epl_post_info['post_image']; ?>' alt='img'>
            <div class="slider_content_box">
                <ul class="post_details simple">
                    <li class="category"><a href="" title="English Premier League">English Premier League</a></li>
                    <li class="date">
                        10:11 PM, Feb 02
                    </li>
                </ul>
                <h2><a href="post_details.php?post_id=<?php echo $epl_post_info['post_id']; ?>" title="<?php echo $epl_post_info['post_title']; ?>"><?php echo $epl_post_info['post_title']; ?></a></h2>

            </div>
        </li>
    <?php } ?>
    <?php while ($laliga_post_info = mysqli_fetch_assoc($query_result_laliga)) { ?>
        <li class="slide">
            <img src='admin/<?php echo $laliga_post_info['post_image']; ?>' alt='img'>
            <div class="slider_content_box">
                <ul class="post_details simple">
                    <li class="category"><a href="" title="Spanish La Liga">Spanish La Liga</a></li>
                    <li class="date">
                        10:11 PM, Feb 02
                    </li>
                </ul>
                <h2><a href="post_details.php?post_id=<?php echo $laliga_post_info['post_id']; ?>" title="<?php echo $laliga_post_info['post_title']; ?>"><?php echo $laliga_post_info['post_title']; ?></a></h2>

            </div>
        </li>
    <?php } ?>
    <?php while ($bangladesh_post_info = mysqli_fetch_assoc($query_result_bangladesh)) { ?>
        <li class="slide">
            <img src='admin/<?php echo $bangladesh_post_info['post_image']; ?>' alt='img'>
            <div class="slider_content_box">
                <ul class="post_details simple">
                    <li class="category"><a href="" title="Bangladesh">Bangladesh</a></li>
                    <li class="date">
                        10:11 PM, Feb 02
                    </li>
                </ul>
                <h2><a href="post_details.php?post_id=<?php echo $bangladesh_post_info['post_id']; ?>" title="<?php echo $bangladesh_post_info['post_title']; ?>"><?php echo $bangladesh_post_info['post_title']; ?></a></h2>

            </div>
        </li>
    <?php } ?>

</ul>


<div class="page">
    <div class='slider_posts_list_container'>
    </div>
    <div class="page_layout page_margin_top clearfix">
        <div class="row">
            <div class="column column_2_3">
               <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Posts From EPL</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while($epl_post_info= mysqli_fetch_assoc($epl_index)) {?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $epl_post_info['post_id']; ?>" title="<?php echo $epl_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $epl_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $epl_post_info['post_id']; ?>" title="<?php echo $epl_post_info['post_title']; ?>"><?php echo $epl_post_info['post_title']; ?></a></h5>
                                <a class="read_more" href="post_details.php?post_id=<?php echo $epl_post_info['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Posts From La Liga</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while($laliga_post_info= mysqli_fetch_assoc($laliga_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $laliga_post_info['post_id']; ?>" title="<?php echo $laliga_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $laliga_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $laliga_post_info['post_id']; ?>" title="<?php echo $laliga_post_info['post_title']; ?>"><?php echo $laliga_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $laliga_post_info['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Post from Champions League</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while ($ucl_post_info= mysqli_fetch_assoc($ucl_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $ucl_post_info['post_id']; ?>" title="<?php echo $ucl_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $ucl_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $ucl_post_info['post_id']; ?>" title="<?php echo $ucl_post_info['post_title']; ?>"><?php echo $ucl_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $ucl_post_info['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                     <h4 class="box_header">Latest Post from Featured</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while ($featured_post_info= mysqli_fetch_assoc($featured_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $featured_post_info['post_id']; ?>" title="<?php echo $featured_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $featured_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $featured_post_info['post_id']; ?>" title="<?php echo $featured_post_info['post_title']; ?>"><?php echo $featured_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $featured_post_info['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Post from World Football</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while ($worldfootball_post_info= mysqli_fetch_assoc($worldfootball_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $worldfootball_post_info['post_id']?>" title="<?php echo $worldfootball_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $worldfootball_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $worldfootball_post_info['post_id']?>" title="<?php echo $worldfootball_post_info['post_title']; ?>"><?php echo $worldfootball_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $worldfootball_post_info['post_id']?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Post from Bangladesh</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while ($bangladesh_post_info= mysqli_fetch_assoc($bangladesh_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $bangladesh_post_info['post_id']?>" title="<?php echo $bangladesh_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $bangladesh_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $bangladesh_post_info['post_id']?>" title="<?php echo $bangladesh_post_info['post_title']; ?>"><?php echo $bangladesh_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="<?php echo $bangladesh_post_info['post_id']?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Post from Legends Column</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while ($LGCL_post_info= mysqli_fetch_assoc($legends_column_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $LGCL_post_info['post_id']?>" title="<?php echo $LGCL_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $LGCL_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $LGCL_post_info['post_id']?>" title="<?php echo $LGCL_post_info['post_title']; ?>"><?php echo $LGCL_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $LGCL_post_info['post_id']?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
               
            </div>
            <div class="column column_1_3">
                <h4 class="box_header">Scores around the leagues</h4>
                <br/>
                <ul class="blog small_margin clearfix">
                    <iframe height="350" frameborder="5" style="vertical-align: bottom;"  src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka&type=timetable#http%3A%2F%2Fwww.dboxb.com%2F"></iframe>
                </ul>
                
                <h4 class="box_header page_margin_top_section">Latest Posts</h4>
                
                <div class="vertical_carousel_container clearfix">
                    <ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                        <?php while ($latest_post_info= mysqli_fetch_assoc($latest_post_index)) { ?>
                        <li class="post">
                            <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>">
                                <img src="admin/<?php echo $latest_post_info['post_image']?>" height="50" width="60" alt='img'>
                            </a>
                            <div class="post_content">
                                <h5>
                                    <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>"><?php echo $latest_post_info['post_title']?></a>
                                </h5>
                                <ul class="post_details simple">
                                    <li class="category"><a href="" title="<?php echo $latest_post_info['category_name']?>"><?php echo $latest_post_info['category_name']?></a></li>
                                    <li class="date">
                                        10:11 PM, Feb 02
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                
                    <h4 class="box_header page_margin_top_section">Authors</h4>
                    <ul class="authors rating clearfix">

                        <?php while ($author_info = mysqli_fetch_array($author_list_index)) { ?>
                        <li class="author">
                            <a class="thumb" href="author_details.php?author_id=<?php echo $author_info['author_id']; ?>" title="<?php echo $author_info['author_name']; ?>">
                                <img src='admin/<?php echo $author_info['author_image']; ?>' height="100" width="120" alt='img'>

                            </a>
                            <div class="details">
                                <h5><a href="author_details.php?author_id=<?php echo $author_info['author_id']; ?>" title="<?php echo $author_info['author_name']; ?>"><?php echo $author_info['author_name']; ?></a></h5>
                                <h6><?php echo $author_info['designation']; ?></h6>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                
                    <h4 class="box_header page_margin_top_section">Categories</h4>
                    <ul class="taxonomies columns clearfix page_margin_top">
                        <?php while($list= mysqli_fetch_assoc($published_category)) {?>
                        <li>
                            <a href="#" title="<?php echo $list['category_name'];?>"><?php echo $list['category_name'];?></a>
                        </li>
                        <?php }?>
                    </ul>
                    
                    <h4 class="box_header page_margin_top_section">Tags</h4>
                    <ul class="taxonomies clearfix page_margin_top">
                        <?php while($tag_list= mysqli_fetch_assoc($tag_result)) {?>
                        <li>
                            <a href="#" title="<?php echo $tag_list['tag_name'];?>"><?php echo $tag_list['tag_name'];?></a>
                        </li>
                        <?php } ?>
                    </ul>
                
                
                
            </div>
        </div>
    </div>
</div>
