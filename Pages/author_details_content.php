<?php

$author_id=$_GET['author_id'];
$author_result= author_info_by_id($author_id);
$author_info= mysqli_fetch_assoc($author_result);

$author_post= select_all_post_by_id($author_id);

$latest_post_index= select_all_latest_post_index();
?>

<div class="page">
    <div class="page_header clearfix page_margin_top">
        <div class="page_header_left">
            <h1 class="page_title"><?php echo $author_info['author_name']?></h1>
        </div>
    </div>
    <div class="page_layout clearfix">
        <div class="divider_block clearfix">
            <hr class="divider first">
            <hr class="divider subheader_arrow">
            <hr class="divider last">
        </div>
        <div class="row page_margin_top">
            <div class="column column_2_3">
                <div class="row">
                    <ul class="authors_list rating">
                        
                        <li class="author clearfix">
                            <div class="avatar_block">
                                <a href="admin/<?php echo $author_info['author_image']?>" class="prettyPhoto" title="<?php echo $author_info['author_name']?>">
                                    <img src='admin/<?php echo $author_info['author_image']?>' height="180" width="220" alt='img'>
                                </a>
                                <div class="details clearfix">
                                    
                                </div>
                            </div>
                            <div class="content">
                                <ul class="social_icons clearfix">
                                    <li>
                                        <a target="_blank" title="" href="<?php echo $author_info['FB']?>" class="social_icon facebook">
                                            &nbsp;
                                        </a>
                                    </li>
                                </ul>
                                
                                <h2><?php echo $author_info['author_name']?></h2>
                                <br>
                                <h6><?php echo $author_info['designation']?></h6>
                                <p><?php echo $author_info['about_author']?></p>
                                <blockquote class="simple">
                                    <?php echo $author_info['fav_quote']?>
                                </blockquote>
                                <br>
                                <br>
                                <h6>Designation: <?php echo $author_info['designation']?></h6>
                                <br>
                                <h6>Email: <?php echo $author_info['author_email']?></h6>
                                <br>
                                <h6>Contact Number: <?php echo $author_info['contact_number']?></h6>
                                <br>
                                <h6>Home Address: <?php echo $author_info['home_address']?></h6>
                                <br>
                                <h6>Joining Date: <?php echo $author_info['joining_date']?></h6>
                            </div>
                        </li>
                        
                    </ul>
                    <div class="row">
                        <div class="row page_margin_top_section">
                    <h4 class="box_header">Latest Posts By <?php echo $author_info['author_name']?></h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            
                            <?php while($author_post_info= mysqli_fetch_assoc($author_post)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $author_post_info['post_id']; ?>" title="<?php echo $author_post_info['post_title']; ?>">
                                    <img src='admin/<?php echo $author_post_info['post_image']; ?>' height="130" width="150" alt='img'>
                                </a>
                                <h5><a href="post_details.php?post_id=<?php echo $author_post_info['post_id']; ?>" title="<?php echo $author_post_info['post_title']; ?>"><?php echo $author_post_info['post_title']; ?></a></h5>
                                <ul class="post_details simple">
                                    <a class="read_more" href="post_details.php?post_id=<?php echo $author_post_info['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                </ul>
                            </li>
                            <?php } ?>
                            
                        </ul>
                    </div>
                </div>
                    </div>
                    
                </div>
            </div>
            <div class="column column_1_3">
                <div class="tabs no_scroll clearfix">
                    <h4 class="box_header">Scores around the leagues</h4>
                    <ul class="tabs_navigation clearfix">
                    
                    <br/>
                    <ul class="blog small_margin clearfix">
                        <iframe height="350" frameborder="5" style="vertical-align: bottom;"  src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka&type=timetable#http%3A%2F%2Fwww.dboxb.com%2F"></iframe>
                    </ul>
                    </ul>
                    
                <h4 class="box_header page_margin_top_section">Latest Posts</h4>
                <div class="vertical_carousel_container clearfix">
                 
                <div class="vertical_carousel_container clearfix">
                    <ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                        <?php while ($latest_post_info= mysqli_fetch_assoc($latest_post_index)) { ?>
                        <li class="post">
                            <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>">
                                <img src="admin/<?php echo $latest_post_info['post_image']?>" height="50" width="60" alt='img'>
                            </a>
                            <div class="post_content">
                                <h5>
                                    <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>"><?php echo $latest_post_info['post_title']?></a>
                                </h5>
                                <ul class="post_details simple">
                                    <li class="category"><a href="" title="<?php echo $latest_post_info['category_name']?>"><?php echo $latest_post_info['category_name']?></a></li>
                                    <li class="date">
                                        10:11 PM, Feb 02
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>