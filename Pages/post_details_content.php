<?php
$post_id = $_GET['post_id'];
$query_result = select_post_info_by_id($post_id);

$post_info = mysqli_fetch_assoc($query_result);

$latest_post_index = select_all_latest_post_index();

$category_post = select_category_post_by_id();
$featured_index = select_featured_post_for_index();
$author_list_index= select_all_author_index();

$cat_result= select_all_published_cat();
$tag_result= select_all_published_tag();
?>

<div class="page">
    <div class="page_layout page_margin_top clearfix">
        <div class="row page_margin_top">
            <div class="column column_1_1">
                <div class="horizontal_carousel_container small">
                    <ul class="blog horizontal_carousel autoplay-1 scroll-1 visible-3 navigation-1 easing-easeInOutQuint duration-750">

                        <?php while ($latest_post_info = mysqli_fetch_assoc($latest_post_index)) { ?>
                            <li class="post">
                                <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id'] ?>" title="<?php echo $latest_post_info['post_title'] ?>">
                                    <img src="admin/<?php echo $latest_post_info['post_image'] ?>" height="200" width="240" alt='img'>
                                </a>
                                <div class="post_content">
                                    <h5>
                                        <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id'] ?>" title="<?php echo $latest_post_info['post_title'] ?>"><?php echo $latest_post_info['post_title'] ?></a>
                                    </h5>
                                    <ul class="post_details simple">
                                        <li class="category"><a href="" title="<?php echo $latest_post_info['category_name'] ?>"><?php echo $latest_post_info['category_name'] ?></a></li>
                                        <li class="date">
                                            10:11 PM, Feb 02
                                        </li>
                                    </ul>
                                </div>
                            </li>
                        <?php } ?>

                    </ul>
                </div>
            </div>
        </div>
        <hr class="divider page_margin_top">
        <div class="row page_margin_top">
            <div class="column column_2_3">
                <div class="row">
                    <div class="post single">
                        <h1 class="post_title">
                            <?php echo $post_info['post_title']; ?>
                        </h1>
                        <ul class="post_details clearfix">
                            <li class="detail category">In <a href="" title="<?php echo $post_info['category_name']; ?>"><?php echo $post_info['category_name']; ?></a></li>
                            <li class="detail date">8:25 PM, Feb 23</li>
                            <li class="detail author">By <a href="author_details.php?author_id=<?php echo $post_info['author_id']; ?>" title="<?php echo $post_info['author_name']; ?>"><?php echo $post_info['author_name']; ?></a></li>
                            <li class="detail views">6 254 Views</li>
                            
                        </ul>
                        <a href="admin/<?php echo $post_info['post_image']; ?>" class="post_image page_margin_top prettyPhoto" title="<?php echo $post_info['post_title']; ?>">
                            <img src='admin/<?php echo $post_info['post_image']; ?>' alt='img'>
                        </a>
                        <div class="sentence">
                            <span class="text"><?php echo $post_info['post_title']; ?></span>
                            <span class="author"><?php echo $post_info['author_name']; ?>, Flickr</span>
                        </div>
                        <div class="post_content page_margin_top_section clearfix">
                            <div class="content_box">
                                <!--<h3 class="excerpt">Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as flood tourists at the site of disasters.</h3>-->
                                <div class="text">
                                    <p><?php echo $post_info['post_description']; ?></p>
                                    <p></p>
<!--<p>Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents for doing little more than turning up as “flood tourists” at the site of disasters, incapable of helping those in crisis and only there for a photo opportunity. The Environment Agency, the body responsible for combating floods and managing rivers, has also been blamed for failing to curb the disasters. But there’s an ever larger debate over the role of climate change in the current floods and storms, and it has been unremittingly hostile.</p>-->
                                    <!--                                    <blockquote class="inside_text page_margin_top">
                                                                            Politicians have looked weak in the face of such natural disaster, with many facing criticism from local residents.
                                                                            <span class="author">&#8212;&nbsp;&nbsp;Julia Slingo, ETF</span>
                                                                        </blockquote>-->
                                                                        <!--<p>For those affected by flooding however, their immediate concerns are not necessarily about the manmade changes to the earth’s atmosphere. A YouGov poll from February found that while 84% of those surveyed believed Britain was likely to experience similar extreme weather events in the next few years, only 30% thought it was connected to man-made climate change.</p>-->
                                                                        <!--<p>There is no evidence to counter the basic premise that a warmer world will lead to more intense daily and hourly rain events. When heavy rain in 2000 devastated parts of Britain, a later study found the climate change had doubled the chances of the flood occurring, said Julia Slingo.</p>-->
                                    
                                </div>
                            </div>
                            <div class="author_box animated_element">
                                <div class="author">
                                    <a title="<?php echo $post_info['author_name']; ?>" href="author_details.php?author_id=<?php echo $post_info['author_id']; ?>" class="thumb">
                                        <img alt="img" src="admin/<?php echo $post_info['author_image']; ?>" height="70" width="70">
                                    </a>
                                    <div class="details">
                                        <h5><a title="<?php echo $post_info['author_name']; ?>" href="author_details.php?author_id=<?php echo $post_info['author_id']; ?>"><?php echo $post_info['author_name']; ?></a></h5>
                                        <h6><?php echo $post_info['designation']; ?></h6>
                                        <a href="author_details.php?author_id=<?php echo $post_info['author_id']; ?>" class="more highlight margin_top_15">PROFILE</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row page_margin_top">
                    <div class="share_box clearfix">
                        <label>Share:</label>
                        <ul class="social_icons clearfix">
                            <li>
                                <a target="_blank" title="" href="http://facebook.com/QuanticaLabs" class="social_icon facebook">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a target="_blank" title="" href="https://twitter.com/QuanticaLabs" class="social_icon twitter">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a title="" href="mailto:contact@pressroom.com" class="social_icon mail">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a title="" href="#" class="social_icon skype">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a title="" href="http://themeforest.net/user/QuanticaLabs?ref=QuanticaLabs" class="social_icon envato">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a title="" href="#" class="social_icon instagram">
                                    &nbsp;
                                </a>
                            </li>
                            <li>
                                <a title="" href="#" class="social_icon pinterest">
                                    &nbsp;
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row page_margin_top">
                    <ul class="taxonomies tags left clearfix">
                        <li>
                            <a href="#" title="<?php echo $post_info['tag_name']; ?>"><?php echo $post_info['tag_name']; ?></a>
                        </li>
                    </ul>
                    <ul class="taxonomies categories right clearfix">
                        <li>
                            <a href="" title="<?php echo $post_info['category_name']; ?>"><?php echo $post_info['category_name']; ?></a>
                        </li>
                    </ul>
                </div>




                <div class="row page_margin_top_section">

                    <h4 class="box_header">RANDOM POSTS FROM ELSEWHERE</h4>
                    <div class="horizontal_carousel_container page_margin_top">
                        <ul class="blog horizontal_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">

                            <?php while ($related_category = mysqli_fetch_assoc($category_post)) { ?>
                                <li class="post">
                                    <a href="post_details.php?post_id=<?php echo $related_category['post_id'] ?>" title="<?php echo $related_category['post_title']; ?>">
                                        <img src='admin/<?php echo $related_category['post_image']; ?>' height="130" width="150" alt='img'>
                                    </a>
                                    <h5><a href="post_details.php?post_id=<?php echo $related_category['post_id'] ?>" title="<?php echo $related_category['post_title']; ?>"><?php echo $related_category['post_title']; ?></a></h5>
                                    <ul class="post_details simple">
                                        <a class="read_more" href="post_details.php?post_id=<?php echo $related_category['post_id'] ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                                    </ul>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>      

                </div>

            </div>
            <div class="column column_1_3">
                <div class="tabs no_scroll clearfix">
                    <h4 class="box_header">Scores around the leagues</h4>
                    <br/>
                    <ul class="blog small_margin clearfix">
                        <iframe height="350" frameborder="5" style="vertical-align: bottom;"  src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka&type=timetable#http%3A%2F%2Fwww.dboxb.com%2F"></iframe>
                    </ul>

                </div>



                
                <div class="row page_margin_top_section">
                    <h4 class="box_header page_margin_top_section">Featured</h4>
                    <div class="vertical_carousel_container clearfix">
                        <ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                            <?php while ($featured_post_info = mysqli_fetch_assoc($featured_index)) { ?>
                                <li class="post">
                                    <a href="post_details.php?post_id=<?php echo $featured_post_info['post_id'] ?>" title="<?php echo $featured_post_info['post_title'] ?>">
                                        <img src="admin/<?php echo $featured_post_info['post_image'] ?>" height="50" width="60" alt='img'>
                                    </a>
                                    <div class="post_content">
                                        <h5>
                                            <a href="post_details.php?post_id=<?php echo $featured_post_info['post_id'] ?>" title="<?php echo $featured_post_info['post_title'] ?>"><?php echo $featured_post_info['post_title'] ?></a>
                                        </h5>
                                        <ul class="post_details simple">
                                            <li class="category"><a href="" title="Featured">Featured</a></li>
                                            <li class="date">
                                                10:11 PM, Feb 02
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                            <?php } ?>

                        </ul>
                    </div>
                </div>



                <h4 class="box_header page_margin_top_section">Authors</h4>
                <ul class="authors rating clearfix">
                    
                    <?php while ($author_info = mysqli_fetch_array($author_list_index)) { ?>
                    <li class="author">
                        <a class="thumb" href="author_details.php?author_id=<?php echo $author_info['author_id']; ?>" title="<?php echo $author_info['author_name']; ?>">
                            <img src='admin/<?php echo $author_info['author_image']; ?>' height="100" width="120" alt='img'>
                            
                        </a>
                        <div class="details">
                            <h5><a href="author_details.php?author_id=<?php echo $author_info['author_id']; ?>" title="<?php echo $author_info['author_name']; ?>"><?php echo $author_info['author_name']; ?></a></h5>
                            <h6><?php echo $author_info['designation']; ?></h6>
                        </div>
                    </li>
                    <?php } ?>
                </ul>
                
                
                <h4 class="box_header page_margin_top_section">Categories</h4>
                <ul class="taxonomies columns clearfix page_margin_top">
                    <?php while($category_list= mysqli_fetch_assoc($cat_result)) {?>
                    <li>
                        <a href="#" title="<?php echo $category_list['category_name'];?>"><?php echo $category_list['category_name'];?></a>
                    </li>
                    <?php }?>
                </ul>
                <h4 class="box_header page_margin_top_section">Tags</h4>
                <ul class="taxonomies clearfix page_margin_top">
                    <?php while($tag_list= mysqli_fetch_assoc($tag_result)) {?>
                    <li>
                        <a href="#" title="<?php echo $tag_list['tag_name'];?>"><?php echo $tag_list['tag_name'];?></a>
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </div>
</div>