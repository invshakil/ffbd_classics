<?php
$bd_page = select_all_bd_post();
$latest_post_index= select_all_latest_post_index();

?>

<div class="page">
    <div class="page_header clearfix page_margin_top">
        <div class="page_header_left">
            <h1 class="page_title">Bangladesh</h1>
        </div>
    </div>
    <div class="page_layout clearfix">
        <div class="divider_block clearfix">
            <hr class="divider first">
            <hr class="divider subheader_arrow">
            <hr class="divider last">
        </div>
        <div class="row">
            <div class="column column_2_3">
                <div class="row">
                    <ul class="blog big">
                        
                        <?php while($bd_post= mysqli_fetch_assoc($bd_page)) { ?>
                        <li class="post">
                            <a href="post_details.php?post_id=<?php echo $bd_post['post_id']; ?>" title="<?php echo $bd_post['post_title']; ?>">
                                <img src='admin/<?php echo $bd_post['post_image']; ?>' height="180" width="220" alt='img'>
                            </a>
                            <div class="post_content">
                                <h2 class="with_number">
                                    <a href="post_details.php?post_id=<?php echo $bd_post['post_id']; ?>" title="<?php echo $bd_post['post_title']; ?>"><?php echo $bd_post['post_title']; ?></a>
                                    
                                </h2>
                                <ul class="post_details">
                                    <li class="category"><a href="" title="Bangladesh">Bangladesh</a></li>
                                    <li class="date">
                                        10:11 PM, Feb 02
                                    </li>
                                </ul>
                                
                                <a class="read_more" href="post_details.php?post_id=<?php echo $bd_post['post_id']; ?>" title="Read more"><span class="arrow"></span><span>READ MORE</span></a>
                            </div>
                        </li>
                        <?php } ?>
                        
                    </ul>
                </div>
                <ul class="pagination clearfix page_margin_top_section">
                    <li class="left">
                        <a href="#" title="">&nbsp;</a>
                    </li>
                    <li class="selected">
                        <a href="#" title="">
                            1
                        </a>
                    </li>
                    <li>
                        <a href="#" title="">
                            2
                        </a>
                    </li>
                    <li>
                        <a href="#" title="">
                            3
                        </a>
                    </li>
                    <li class="right">
                        <a href="#" title="">&nbsp;</a>
                    </li>
                </ul>
            </div>
            <div class="column column_1_3 page_margin_top">
                <div class="tabs no_scroll clearfix">
                    <h4 class="box_header">Scores around the leagues</h4>
                    <br/>
                    <ul class="blog small_margin clearfix">
                        <iframe height="350" frameborder="5" style="vertical-align: bottom;"  src="//tools.whatsthescore.com/tools.php?id=37615&iframe=tap-sport-tools-37615&tz-js=Asia/Dhaka&type=timetable#http%3A%2F%2Fwww.dboxb.com%2F"></iframe>
                    </ul>
                </div>
                
                <h4 class="box_header page_margin_top_section">Latest Posts</h4>
                <div class="vertical_carousel_container clearfix">
                    <ul class="blog small vertical_carousel autoplay-1 scroll-1 navigation-1 easing-easeInOutQuint duration-750">
                        
                      <?php while ($latest_post_info= mysqli_fetch_assoc($latest_post_index)) { ?>
                        <li class="post">
                            <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>">
                                <img src="admin/<?php echo $latest_post_info['post_image']?>" height="50" width="60" alt='img'>
                            </a>
                            <div class="post_content">
                                <h5>
                                    <a href="post_details.php?post_id=<?php echo $latest_post_info['post_id']?>" title="<?php echo $latest_post_info['post_title']?>"><?php echo $latest_post_info['post_title']?></a>
                                </h5>
                                <ul class="post_details simple">
                                    <li class="category"><a href="" title="<?php echo $latest_post_info['category_name']?>"><?php echo $latest_post_info['category_name']?></a></li>
                                    <li class="date">
                                        10:11 PM, Feb 02
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <?php } ?>  
                        
                    </ul>
                </div>
                
            </div>
        </div>
    </div>
</div>