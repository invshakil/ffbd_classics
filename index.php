<?php
ob_start();
require ('functions.php');

?>
<!DOCTYPE html>
<html>
    <head>
        <title>FFBD HUB</title>
        <!--meta-->
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.2" />
        <meta name="format-detection" content="telephone=no" />
        <meta name="keywords" content="Medic, Medical Center" />
        <meta name="description" content="Responsive Medical Health Template" />
        <!--style-->
        <link href='//fonts.googleapis.com/css?family=Roboto:300,400,700' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="style/reset.css">
        <link rel="stylesheet" type="text/css" href="style/superfish.css">
        <link rel="stylesheet" type="text/css" href="style/prettyPhoto.css">
        <link rel="stylesheet" type="text/css" href="style/jquery.qtip.css">
        <link rel="stylesheet" type="text/css" href="style/style.css">
        <link rel="stylesheet" type="text/css" href="style/menu_styles.css">
        <link rel="stylesheet" type="text/css" href="style/animations.css">
        <link rel="stylesheet" type="text/css" href="style/responsive.css">
        <!--<link rel="stylesheet" type="text/css" href="style/odometer-theme-default.css">-->
        <link rel="stylesheet" type="text/css" href="style/dark_skin.css">
        <!--<link rel="stylesheet" type="text/css" href="style/high_contrast_skin.css">-->
        <link rel="shortcut icon" href="images/favicon.ico">

    </head>
    <!--<body class="image_1">
    <body class="image_1 overlay">
    <body class="image_2">
    <body class="image_2 overlay">
    <body class="image_3">
    <body class="image_3 overlay">
    <body class="image_4">
    <body class="image_4 overlay">
    <body class="image_5">
    <body class="image_5 overlay">
    <body class="pattern_1">
    <body class="pattern_2">
    <body class="pattern_3">
    <body class="pattern_4">
    <body class="pattern_5">
    <body class="pattern_6">
    <body class="pattern_7">
    <body class="pattern_8">
    <body class="pattern_9">
    <body class="pattern_10">-->
    <body>
        <div class="site_container">
            <!--<div class="header_top_bar_container style_2 clearfix">
            <div class="header_top_bar_container style_2 border clearfix">
            <div class="header_top_bar_container style_3 clearfix">
            <div class="header_top_bar_container style_3 border clearfix">
            <div class="header_top_bar_container style_4 clearfix">
            <div class="header_top_bar_container style_4 border clearfix">
            <div class="header_top_bar_container style_5 clearfix">
            <div class="header_top_bar_container style_5 border clearfix"> -->
<?php include './include/top_header.php'; ?>
            <?php include './include/header_container.php'; ?>
            <!--<div class="header_container small">
            <div class="header_container style_2">
            <div class="header_container style_2 small">
            <div class="header_container style_3">
            <div class="header_container style_3 small">-->

            <!-- <div class="menu_container style_2 clearfix">
            <div class="menu_container style_3 clearfix">
            <div class="menu_container style_... clearfix">
            <div class="menu_container style_10 clearfix">
            <div class="menu_container sticky clearfix">-->
<?php include './include/nav_menu.php'; ?>			
            <!--Start of Mastering--->
            <div class="page">
<?php
if (isset($pages)) {
    if ($pages == 'epl') {
        include './Pages/epl_content.php';
    } else if ($pages == 'fixture') {
        include './Pages/fixture_content.php';
    } else if ($pages == 'point_table') {
        include './Pages/point_table_content.php';
    } else if ($pages == 'bundesliga') {
        
    } else if ($pages == 'laliga') {
        include './Pages/laliga_content.php';
    } else if ($pages == 'german_league') {
        include './Pages/german_league_content.php';
    } else if ($pages == 'ucl') {
        include './Pages/ucl_content.php';
    } else if ($pages == 'articles') {
        include './Pages/articles_content.php';
    } else if ($pages == 'bangladesh') {
        include './Pages/bangladesh_content.php';
    } else if ($pages == 'legends_columnn') {
        include './Pages/legends_columnn_content.php';
    } else if ($pages == 'video_archive') {
        include './Pages/video_archive_content.php';
    } else if ($pages == 'author_list') {
        include './Pages/author_list_content.php';
    } else if ($pages == 'author_details') {
        include './Pages/author_details_content.php';
    } else if ($pages == 'image_gallary') {
        include './Pages/image_gallary_content.php';
    } else if ($pages == 'about_us') {
        include './Pages/about_us_content.php';
    } else if ($pages == 'post_details') {
        include './Pages/post_details_content.php';
    }
} else {
    include './Pages/index_content.php';
}
?>
            </div>
            <!--End of Mastering--->
            <div class="footer_container">
                <?php include './include/footer.php'; ?>
            </div>
        </div>
        <div class="background_overlay"></div>
        <!--js-->
        <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="js/jquery-migrate-1.2.1.min.js"></script>
        <script type="text/javascript" src="js/jquery.ba-bbq.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.11.1.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
        <script type="text/javascript" src="js/jquery.carouFredSel-6.2.1-packed.js"></script>
        <script type="text/javascript" src="js/jquery.touchSwipe.min.js"></script>
        <script type="text/javascript" src="js/jquery.transit.min.js"></script>
        <script type="text/javascript" src="js/jquery.sliderControl.js"></script>
        <script type="text/javascript" src="js/jquery.timeago.js"></script>
        <script type="text/javascript" src="js/jquery.hint.js"></script>
        <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="js/jquery.qtip.min.js"></script>
        <script type="text/javascript" src="js/jquery.blockUI.js"></script>
        <script type="text/javascript" src="//maps.google.com/maps/api/js?sensor=false"></script>
        <script type="text/javascript" src="js/main.js"></script>
        <script type="text/javascript" src="js/odometer.min.js"></script>
    </body>
</html>