<?php
require 'functions.php';


if (isset($_GET['name'])) {
    $id = $_GET['id'];
    $message = delete_data($id);
}

$query_result = select_all_user_info();
?>

<!-- Default datatable inside panel -->
<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-table" align="center"></i>Admin Panel</h6></div>
    <h3 style="color: red; text-align: center;">
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
        }
        ?>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
    </h3>
    <div class="datatable">
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Admin Name</th>
                    <th>Email Address</th>
                    <th>Access Level</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        <td><?php echo $row['admin_id']; ?></td>
                        <td><?php echo $row['admin_name']; ?></td>
                        <td><?php echo $row['email_address']; ?></td>
                        <td>
                            <?php
                            if ($row['access_level'] == 1) {
                                echo $row['access_level'] = 'Super Admin';
                            } else {
                                echo $row['access_level'] = 'Admin';
                            }
                            ?></td>
                        <td>
                            <a title="Edit Information" class="icon-pencil3" href="update_admin_user.php?id=<?php echo $row['admin_id']; ?>">
        <!--                                    <i class="halflings-icon white edit"></i>  -->
                            </a>
                            &nbsp
                            <a title="Delete This Admin" class="icon-remove2"  href="?name=delete&id=<?php echo $row['admin_id']; ?>" onclick="return checkDelete();"></a>
    <!--                                    <i class="halflings-icon white trash"></i> -->
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
