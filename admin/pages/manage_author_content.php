<?php
require 'functions.php';

if (isset($_GET['name'])) {
    $id = $_GET['id'];



    $message = delete_author($id);
}

$query_result = select_all_author_info();
?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-table" align="center"></i>Author List</h6></div>
    <h3 style="color: red; text-align: center;">
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
    </h3>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Author Name</th>
                    <th>Contact Number</th>
                    <th>Author Email</th>
                    <th>Joining Date</th>
                    <th>Home Address</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        <td><?php echo $row['author_id']; ?></td>
                        <td><?php echo $row['author_name']; ?></td>
                        <td><?php echo $row['contact_number']; ?></td>
                        <td><?php echo $row['author_email']; ?></td>
                        <td><?php echo $row['joining_date']; ?></td>
                        <td><?php echo $row['home_address']; ?></td>
                        
                        <td>
                            <a class="icon-pencil3" href="update_author.php?id=<?php echo $row['author_id']; ?>">
        <!--                                    <i class="halflings-icon white edit"></i>  -->
                            </a>
                            &nbsp
                            <a class="icon-remove2"  href="?name=delete&id=<?php echo $row['author_id']; ?>" onclick="return checkDelete();"></a>
    <!--                                    <i class="halflings-icon white trash"></i> -->
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
