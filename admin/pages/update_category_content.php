<?php
$category_id = $_GET['id'];
require 'functions.php';
$result = show_category_info($category_id);
$result_1 = mysqli_fetch_assoc($result);
//echo '<pre>';
//print_r($result_f);
//exit();


if(isset($_POST['btn'])) {
 $message=update_category_info($_POST);  
}

?>

<!-- Form bordered -->
<form name="edit_category_form" class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Create Category</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Category Name:</label>
                <div class="col-sm-10">
                    <input type="text" name="category_name" class="form-control"
                           value="<?php echo $result_1['category_name']; ?>">
                    <input type="hidden" name="category_id" class="form-control"
                           value="<?php echo $result_1['category_id']; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Category Description:</label>
                <div class="col-sm-10">
                    <!-- WYSIWYG editor inside panel -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="block-inner">
                                <textarea name="category_description" class="editor" placeholder="Enter description about this category ..."><?php echo $result_1['category_description']; ?></textarea>
                            </div></div>
                    </div>
                    <!-- /WYSIWYG editor inside panel -->
                </div>
            </div>

            <div class="form-group" >
                <label class="col-sm-2 control-label">Publication Status: </label>
                <select data-placeholder="Choose Publication Status" name="publication_status" class="col-sm-10 select-full" >
                    <option >--Select Publication Status--</option> 
                    <option value="1">Published</option> 
                    <option value="2">Pending</option> 
                </select>
            </div>

            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Update Category" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->

<script>
    document.forms['edit_category_form'].elements['publication_status'].value='<?php echo $result_1['publication_status']; ?>';
</script>