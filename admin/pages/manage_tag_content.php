<?php
require 'functions.php';

if (isset($_GET['name'])) {
    $id = $_GET['id'];



    $message = delete_tag($id);
}

$query_result = select_all_tag_info();
?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-table" align="center"></i>Tag List</h6></div>
    <h3 style="color: red; text-align: center;">
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
    </h3>
        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Tag Name</th>
                    <th>Short Description</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        <td><?php echo $row['tag_id']; ?></td>
                        <td><?php echo $row['tag_name']; ?></td>
                        <td><?php echo $row['short_description']; ?></td>
                        <td>
                            <a class="icon-pencil3" href="update_tag.php?id=<?php echo $row['tag_id']; ?>">
                            </a>
                            &nbsp
                            <a class="icon-remove2"  href="?name=delete&id=<?php echo $row['tag_id']; ?>" onclick="return checkDelete();"></a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
