<?php
if (isset($_POST['btn'])) {
    require 'functions.php';
    $message = save_category_info($_POST);
}
?>

<!-- Form bordered -->
<form class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Create Category</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Category Name:</label>
                <div class="col-sm-10">
                    <input required type="text" name="category_name" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Category Description:</label>
                <div class="col-sm-10">
                    <!-- WYSIWYG editor inside panel -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="block-inner">
                                <textarea name="category_description" class="editor" placeholder="Enter description about this category ..."></textarea>
                            </div></div>
                    </div>
                    <!-- /WYSIWYG editor inside panel -->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Publication Status: </label>
                <div class="col-sm-10">
                    <select name="publication_status" class="multi-select" tabindex="2">
                        <option >--Select Publication Status--</option> 
                        <option value="1">Published</option> 
                        <option value="2">Pending</option>
                    </select>
                </div>
            </div>

            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Create Category" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->

