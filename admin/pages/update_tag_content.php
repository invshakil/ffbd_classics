<?php
$tag_id = $_GET['id'];
require 'functions.php';
$result = show_tag_info($tag_id);
$result_1 = mysqli_fetch_assoc($result);
//echo '<pre>';
//print_r($result_f);
//exit();


if(isset($_POST['btn'])) {
 $message=update_tag_info($_POST);  
}

?>

<!-- Form bordered -->
<form class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Update Tag Info</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Tag Name:</label>
                <div class="col-sm-10">
                    <input type="text" name="tag_name" class="form-control"
                           value="<?php echo $result_1['tag_name']; ?>">
                    <input type="hidden" name="tag_id" class="form-control"
                           value="<?php echo $result_1['tag_id']; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Short Description:</label>
                <div class="col-sm-10">
                   <input type="text" name="short_description" class="form-control"
                           value="<?php echo $result_1['short_description']; ?>">
                </div>
            </div>
           
            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Update Info" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->
