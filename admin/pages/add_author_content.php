<?php
if (isset($_POST['btn'])) {
    require 'functions.php';
    $message = save_author_info($_POST);
}
?>

<!-- Form bordered -->
<form class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Add New Author</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Author Name:</label>
                <div class="col-sm-10">
                    <input required type="text" name="author_name" class="form-control" placeholder="enter name">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">About Author:</label>
                <div class="col-sm-10">
                    <input required type="text" name="about_author" class="form-control" placeholder="About Yourself">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Favorite Quote:</label>
                <div class="col-sm-10">
                    <input required type="text" name="fav_quote" class="form-control" placeholder="Favorite Quote">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Attach Image:</label>
                <div class="col-sm-10">
                    <input required type="file" name="author_image" class="styled" id="attach" >
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Contact Number:</label>
                <div class="col-sm-10">
                    <input required type="text" name="contact_number" class="form-control" placeholder="enter your contact name with your area code">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Author Email:</label>
                <div class="col-sm-10">
                    <input required type="email" name="author_email" class="form-control" placeholder="enter your valid email id">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">FB ID Link:</label>
                <div class="col-sm-10">
                    <input required type="text" name="FB" class="form-control" placeholder="Facebook ID link">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Joining Date:</label>
                <div class="col-sm-10">
                    <input required type="text" id="datepicker" name="joining_date" class="form-control" placeholder="drop the cursor on input box and select date">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Designation:</label>
                <div class="col-sm-10">
                    <input required type="text" name="designation" class="form-control" placeholder="Designation">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Home Address:</label>
                <div class="col-sm-10">
                    <input required type="text" name="home_address" class="form-control" placeholder="valid address with postal code">
                </div>
            </div>

           

            <div class="form-actions text-right">
                <input required type="submit" name="btn" value="Add Author" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->

