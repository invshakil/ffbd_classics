<?php
ob_start();

require 'functions.php';

if (isset($_POST['btn'])) {
    $message = save_new_post($_POST);
}

$query_category = select_all_published_category();
$query_author = select_all_author();
$query_tag = select_all_tags();
?>

<!-- Form bordered -->
<form class="form-horizontal form-bordered" action="" role="form" method="post" enctype="multipart/form-data">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Create New Post</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Post Title</label>
                <div class="col-sm-10">
                    <input required type="text" name="post_title" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Post Description:</label>
                <div class="col-sm-10">
                    <!-- WYSIWYG editor inside panel -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="block-inner">
                                <textarea name="post_description" class="editor" placeholder="Enter description about this post ..."></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /WYSIWYG editor inside panel -->
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Attach Image:</label>
                <div class="col-sm-10">
                    <input required type="file" name="post_image" class="styled" id="attach" >
                </div>
            </div>
           
            <div class="form-group" >
                <label class="col-sm-2 control-label">Author Name: </label>
                <div class="col-sm-10">
                    <select name="author_id" class="multi-select" tabindex="2">
                        <option >--Select Author Name--</option> 
                        <?php while ($author_info = mysqli_fetch_assoc($query_author)) { ?> 
                            <option value="<?php echo $author_info['author_id'] ?>"><?php echo $author_info['author_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Post Category: </label>
                <div class="col-sm-10">
                    <select name="category_id" class="multi-select" tabindex="2">
                        <option >--Select Category Name--</option> 
                        <?php while ($category_info = mysqli_fetch_assoc($query_category)) { ?> 
                            <option value="<?php echo $category_info['category_id'] ?>"><?php echo $category_info['category_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Select Post Tag: </label>
                <div class="col-sm-10">
                    <select name="tag_id" class="multi-select" tabindex="2">

                        <option >--Select Tags--</option> 
                        <?php while ($tag_info = mysqli_fetch_assoc($query_tag)) { ?> 
                            <option value="<?php echo $tag_info['tag_id'] ?>"><?php echo $tag_info['tag_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Publication Status: </label>
                <div class="col-sm-10">
                    <select name="publication_status" class="multi-select" tabindex="2">
                        <option >--Select Publication Status--</option> 
                        <option value="1">Published</option> 
                        <option value="2">Pending</option>
                    </select>
                </div>
            </div>



            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Submit Post" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->

