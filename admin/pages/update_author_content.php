<?php
$author_id = $_GET['id'];
require 'functions.php';
$result = show_author_info($author_id);
$result_1 = mysqli_fetch_assoc($result);
//echo '<pre>';
//print_r($result_f);
//exit();


if(isset($_POST['btn'])) {
 $message=update_author_info($_POST);  
}

?>

<!-- Form bordered -->
<form name="edit_author_form" class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Update Author Info</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Author Name:</label>
                <div class="col-sm-10">
                    <input type="text" name="author_name" class="form-control"
                           value="<?php echo $result_1['author_name']; ?>">
                    <input type="hidden" name="author_id" class="form-control"
                           value="<?php echo $result_1['author_id']; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Contact Number:</label>
                <div class="col-sm-10">
                   <input type="text" name="contact_number" class="form-control"
                           value="<?php echo $result_1['contact_number']; ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Author Email:</label>
                <div class="col-sm-10">
                   <input type="text" name="author_email" class="form-control"
                           value="<?php echo $result_1['author_email']; ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Joining Date:</label>
                <div class="col-sm-10">
                    <input type="text" id="datepicker" name="joining_date" class="form-control"
                           value="<?php echo $result_1['joining_date']; ?>">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Home Address:</label>
                <div class="col-sm-10">
                   <input type="text" name="home_address" class="form-control"
                           value="<?php echo $result_1['home_address']; ?>">
                </div>
            </div>

            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Update Info" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->
