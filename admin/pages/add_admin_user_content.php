<?php

    if(isset($_POST['btn'])) {
        require 'functions.php';
        $message=save_admin_user_info($_POST);
    }


?>
<div class="panel-body"> 
    <form class="validate" action="" role="form" method="post">

        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
            <span class="text-semibold">Create New Admin</span>
        </div>
        <h3 align="center" style="color: red;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>

        <div class="well" class="form-group">
            <div class="form-group has-feedback" >
                <label>Admin Name: </label>
                <input required type="text" name="admin_name" class="form-control" placeholder="admin_name" class="required form-control">
                <i class="icon-user form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback" >
                <label>Email: </label>
                <input required type="email" name="email_address" class="form-control" placeholder="your@mail.com" class="required form-control">
                <i class="icon-mail form-control-feedback"></i>
            </div>

            <div class="form-group has-feedback" >
                <label>Password: </label>
                <input required type="password" name="password" class="form-control" placeholder="Password" class="required form-control">
                <i class="icon-lock form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback" >
                <label>Access Level: </label>
                <select data-placeholder="Choose access Level" name="access_level" class="select-full" tabindex="2" class="required form-control">
                    <option >--Select Access Level--</option> 
                    <option value="1">Super Admin</option> 
                    <option value="2">Admin</option> 
                </select>
            </div>


            <div class="row form-actions">
                <div class="col-xs-8" >
                    <button type="submit" name="btn" class="btn btn-warning pull-right"><i class="icon-menu2"></i>Create Admin</button>
                </div>
            </div>
        </div>
    </form>
</div>