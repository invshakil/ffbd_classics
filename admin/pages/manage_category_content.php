<?php
require 'functions.php';

if (isset($_GET['name'])) {
    $id = $_GET['id'];



    $message = delete_category($id);
}

$query_result = select_all_category_info();
?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-table" align="center"></i>Manage Category</h6></div>
    <h3 style="color: red; text-align: center;">
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
    </h3>
        <table class="table">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Category Name</th>
                    <th>Category Description</th>
                    <th>Publication Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        <td><?php echo $row['category_id']; ?></td>
                        <td><?php echo $row['category_name']; ?></td>
                        <td><?php echo $row['category_description']; ?></td>
                        <td>
                            <?php
                            if ($row['publication_status'] == 1) {
                                echo $row['publication_status'] = '<button class="btn btn-success">Published</button>';
                            } else {
                                echo $row['publication_status'] = '<button  class="btn btn-danger">Pending</button>';
                            }
                            ?></td>
                        <td>
                            <a class="icon-pencil3" href="update_category.php?id=<?php echo $row['category_id']; ?>">
        <!--                                    <i class="halflings-icon white edit"></i>  -->
                            </a>
                            &nbsp
                            <a class="icon-remove2"  href="?name=delete&id=<?php echo $row['category_id']; ?>" onclick="return checkDelete();"></a>
    <!--                                    <i class="halflings-icon white trash"></i> -->
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
