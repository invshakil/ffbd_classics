<?php
if (isset($_POST['btn'])) {
    require 'functions.php';
    $message = save_tag_info($_POST);
}
?>

<!-- Form bordered -->
<form class="form-horizontal form-bordered" action="" role="form" method="post">
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Add New Tag</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Tag Name:</label>
                <div class="col-sm-10">
                    <input required type="text" name="tag_name" class="form-control" placeholder="enter name">
                </div>
            </div>
            
            <div class="form-group">
                <label class="col-sm-2 control-label">Short Description:</label>
                <div class="col-sm-10">
                    <input required type="text" name="short_description" class="form-control" placeholder="enter tag information in short form">
                </div>
            </div>
            
            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Add Tag" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->

