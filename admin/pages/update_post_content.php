<?php
$post_id = $_GET['id'];
require 'functions.php';
$result = show_posts_info_by_id($post_id);
$result_1 = mysqli_fetch_assoc($result);
//echo '<pre>';
//print_r($result_f);
//exit();

if(isset($_POST['btn'])) {
 $message=  update_posts_info($_POST);  
}

$query_category = select_all_published_category();
$query_author = select_all_author();
$query_tag = select_all_tags();

?>


<!-- Form bordered -->
<form name="edit_post_form" class="form-horizontal form-bordered" action="" role="form" method="post"enctype="multipart/form-data" >
    <div class="panel panel-default">
        <div class="panel-heading" ><h6 class="panel-title" ><i class="icon-menu"></i>Update Post Info</h6></div>
        <h3 style="color: red; text-align: center;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>
        <div class="panel-body">

            <div class="form-group">
                <label class="col-sm-2 control-label">Post Title</label>
                <div class="col-sm-10">
                    <input type="text" name="post_title" class="form-control"
                           value="<?php echo $result_1['post_title']; ?>">
                    <input type="hidden" name="post_id" class="form-control"
                           value="<?php echo $result_1['post_id']; ?>">
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Post Description:</label>
                <div class="col-sm-10">
                    <!-- WYSIWYG editor inside panel -->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="block-inner">
                                <textarea name="post_description" class="editor" placeholder="Enter description about this post ..."><?php echo $result_1['post_description']; ?></textarea>
                            </div>
                        </div>
                    </div>
                    <!-- /WYSIWYG editor inside panel -->
                </div>
            </div>
            
           
            <div class="form-group">
                <label class="col-sm-2 control-label">Author Name: </label>
                <div class="col-sm-10">
                    <select name="author_id" class="multi-select" tabindex="2">
                        <option >--Select Author Name--</option> 
                        <?php while ($author_info = mysqli_fetch_assoc($query_author)) { ?> 
                            <option value="<?php echo $author_info['author_id'] ?>"><?php echo $author_info['author_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-2 control-label">Post Category: </label>
                <div class="col-sm-10">
                    <select name="category_id" class="multi-select" tabindex="2">
                        <option >--Select Category Name--</option> 
                        <?php while ($category_info = mysqli_fetch_assoc($query_category)) { ?> 
                            <option value="<?php echo $category_info['category_id'] ?>"><?php echo $category_info['category_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Select Post Tag: </label>
                <div class="col-sm-10">
                    <select name="tag_id" class="multi-select" tabindex="2">

                        <option >--Select Tags--</option> 
                        <?php while ($tag_info = mysqli_fetch_assoc($query_tag)) { ?> 
                            <option value="<?php echo $tag_info['tag_id'] ?>"><?php echo $tag_info['tag_name'] ?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">Publication Status: </label>
                <div class="col-sm-10">
                    <select name="publication_status" class="multi-select" tabindex="2">
                        <option >--Select Publication Status--</option> 
                        <option value="1">Published</option> 
                        <option value="2">Pending</option>
                    </select>
                </div>
            </div>



            <div class="form-actions text-right">
                <input type="submit" name="btn" value="Update Post Info" class="btn btn-primary">
            </div>

        </div>
    </div>
</form>
<!-- /form striped -->


<script>
    document.forms['edit_post_form'].elements['author_id'].value='<?php echo $result_1['author_id']; ?>';
    document.forms['edit_post_form'].elements['category_id'].value='<?php echo $result_1['category_id']; ?>';
    document.forms['edit_post_form'].elements['tag_id'].value='<?php echo $result_1['tag_id']; ?>';
    document.forms['edit_post_form'].elements['publication_status'].value='<?php echo $result_1['publication_status']; ?>';
</script>