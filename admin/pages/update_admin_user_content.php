<?php
$admin_id = $_GET['id'];
require 'functions.php';
$result = show_admin_user_info($admin_id);
$result_f = mysqli_fetch_assoc($result);
//echo '<pre>';
//print_r($result_f);
//exit();


if(isset($_POST['btn'])) {
 $message=update_admin_user_info($_POST);  
}
?>
<div class="panel-body"> 
    <form name="edit_admin_form" action="" role="form" method="post">

        <div class="popup-header">
            <a href="#" class="pull-left"><i class="icon-user-plus"></i></a>
            <span class="text-semibold">Update Admin User Information</span>
        </div>
        <h3 align="center" style="color: red;">
            <?php
            if (isset($message)) {
                echo $message;
                unset($message);
            }
            ?>

        </h3>

        <div class="well" class="form-group">
            <div class="form-group has-feedback" >
                <label>Admin Name: </label>
                <input type="text" name="admin_name" class="form-control" placeholder="admin_name" 
                       value="<?php echo $result_f['admin_name']; ?>" >
                <input type="hidden" name="admin_id" class="form-control" placeholder="admin_id" 
                       value="<?php echo $result_f['admin_id']; ?>" >
                <i class="icon-user form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback" >
                <label>Email: </label>
                <input type="email" name="email_address" class="form-control" placeholder="your@mail.com" 
                       value="<?php echo $result_f['email_address']; ?>">
                <i class="icon-mail form-control-feedback"></i>
            </div>

            <div class="form-group has-feedback" >
                <label>Password: </label>
                <input type="password" name="password" class="form-control" placeholder="Password"
                       value="<?php echo $result_f['password']; ?>" >
                <i class="icon-lock form-control-feedback"></i>
            </div>
            <div class="form-group has-feedback" >
                <label>Access Level: </label>
                <select data-placeholder="Choose access Level" name="access_level" class="select-full" tabindex="2" >
                    <option >--Select Access Level--</option> 
                    <option value="1">Super Admin</option> 
                    <option value="2">Admin</option> 
                </select>
            </div>


            <div class="row form-actions">
                <div class="col-xs-8" >
                    <button type="submit" name="btn" class="btn btn-warning pull-right"><i class="icon-menu2"></i>Update Admin Info</button>
                </div>
            </div>
        </div>
    </form>
</div>
<!--JavaScript for showing access level from database-->
<script>
    document.forms['edit_admin_form'].elements['access_level'].value='<?php echo $result_f['access_level']; ?>';
</script>