<?php
require 'functions.php';


if (isset($_GET['name'])) {
    $id = $_GET['id'];
    $message = delete_posts_data($id);
}

$query_result = select_all_posts_info();
?>

<div class="panel panel-default">
    <div class="panel-heading"><h6 class="panel-title"><i class="icon-file"></i>Manage Posts</h6></div>
    <h3 style="color: red; text-align: center;">
        <?php
        if (isset($message)) {
            echo $message;
        }
        ?>
        <?php
        if (isset($_SESSION['message'])) {
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        ?>
    </h3>
    <div class="datatable">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>Author</th>
                    <th>Category</th>
                    <th>Tags</th>
                    <th>Publication Status</th>
                    <th>Actions</th>
                </tr>
            </thead>

            <tbody>
                <?php while ($row = mysqli_fetch_assoc($query_result)) { ?>
                    <tr>
                        <td><?php echo $row['post_id']; ?></td>
                        <td><?php echo $row['post_title']; ?></td>
                        <td><?php echo $row['post_description']; ?></td>
                        <td>
                            <?php
                            echo $row['author_name'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $row['category_name'];
                            ?>
                        </td>
                        <td>
                            <?php
                            echo $row['tag_name'];
                            ?>
                        </td>
                        <td>
                            <?php
                            if ($row['publication_status'] == 1) {
                                echo $row['publication_status'] = '<button class="btn btn-success">Published</button>';
                            } else {
                                echo $row['publication_status'] = '<button  class="btn btn-danger">Pending</button>';
                            }
                            ?>
                        </td>
                        <td>
                            <a title="Edit Information" class="icon-pencil3" href="update_post.php?id=<?php echo $row['post_id']; ?>">
                            </a>
                            &nbsp
                            <a title="Delete This Admin" class="icon-remove2"  href="?name=delete&id=<?php echo $row['post_id']; ?>" onclick="return checkDelete();"></a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
            </tbody>
        </table>
    </div>
</div>
