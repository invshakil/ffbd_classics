<?php

ob_start();

//admin login 

function admin_login_check($data) {
    require_once 'db_connect.php';
    $password = md5($data['password']);
    $sql = "SELECT * FROM tbl_admin WHERE email_address='$data[email_address]' AND password='$password' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        $admin_info = mysqli_fetch_assoc($query_result);

        if ($admin_info) {
            $_SESSION['admin_id'] = $admin_info['admin_id'];
            $_SESSION['admin_name'] = $admin_info['admin_name'];
            $_SESSION['access_level'] = $admin_info['access_level'];
            header('Location: admin_master.php');
        } else {
            $message = "Please use valid email address and password";
            return $message;
        }
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}



//Add Admin 

function save_admin_user_info($data) {
    require 'db_connect.php';
    $password = md5($data['password']);
    $sql = "INSERT INTO tbl_admin (admin_name, email_address, password, access_level) VALUES ('$data[admin_name]', '$data[email_address]', '$password', '$data[access_level]')";
    if (mysqli_query($db_connect, $sql)) {
        $message = "Admin user info save successfully";
        return $message;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//selecting admin info from database

function select_all_user_info() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_admin WHERE deletion_status=1 ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//showing admin info at dashboard

function show_admin_user_info($admin_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_admin WHERE admin_id='$admin_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

//update admin user info

function update_admin_user_info($data) {
    include 'db_connect.php';
    $password = md5($data['password']);
    $sql = "UPDATE tbl_admin SET admin_name='$data[admin_name]', email_address='$data[email_address]', password='$password', access_level='$data[access_level]]' WHERE admin_id='$data[admin_id]'";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Admin user info update successfully";
        header('Location: manage_admin_user.php');
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

//delete admin info

function delete_data($id) {
    include 'db_connect.php';
    //$sql = "DELETE FROM tbl_admin WHERE admin_id='$id'";
    $sql = "UPDATE tbl_admin SET deletion_status=0 WHERE admin_id='$id'";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Deleted Successfully";
        header('Location: manage_admin_user.php');
        return $message;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

//Create, Update, Delete Category ----------Start-------------

function save_category_info($data) {
    require 'db_connect.php';
    $sql = "INSERT INTO tbl_category (category_name, category_description, publication_status) VALUES ('$data[category_name]', '$data[category_description]', '$data[publication_status]' )";
    if (mysqli_query($db_connect, $sql)) {
        $message = "Category Info Save Successfully";
        return $message;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_category_info() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_category WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function update_category_info($data) {
    include 'db_connect.php';

    $sql = "UPDATE tbl_category SET category_name='$data[category_name]',category_description='$data[category_description]', publication_status='$data[publication_status]]' WHERE category_id='$data[category_id]'";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Category info updated successfully";
        header('Location: manage_category.php');
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

function show_category_info($category_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_category WHERE category_id='$category_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function delete_category($id) {
    include 'db_connect.php';
    $sql = "UPDATE tbl_category SET deletion_status=0 WHERE category_id='$id' ";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Deleted Successfully";
        header('Location: manage_category.php');
        return $message;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

//Create, Update, Delete Category ----------END-------------//
//
//
//
//---------Add Post, View Post------Start-----------//

function save_new_post($data) {
    require 'db_connect.php';

    $directory = 'post_image/';
    $target_file = $directory . $_FILES['post_image']['name'];
    $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
    $file_size = $_FILES['post_image']['size'];
    $check = getimagesize($_FILES['post_image']['tmp_name']);
    if ($check) {
        if (file_exists($target_file)) {
            echo 'This file is already exists. please try new one';
        } else {
            if ($file_size > 1000000) {
                echo 'File is too large. please try new one';
            } else {
                if ($file_type != 'jpeg' && $file_type != 'jpg' && $file_type != 'png') {
                    echo 'File type is not valid. please try new one';
                } else {
                    move_uploaded_file($_FILES['post_image']['tmp_name'], $target_file);
                    $post_title = addslashes($data['post_title']);
                    $post_description = addslashes($data['post_description']);
                    $sql = "INSERT INTO tbl_posts (post_title, post_description, post_image, author_id, category_id, tag_id, publication_status) VALUES ('$post_title', '$post_description', '$target_file', '$data[author_id]', '$data[category_id]', '$data[tag_id]', '$data[publication_status]' ) ";
                    if (mysqli_query($db_connect, $sql)) {
                        $message = "New Post Created!";
                        return $message;
                    } else {
                        die('Query problem' . mysqli_error($db_connect));
                    }
                }
            }
        }
    } else {
        echo 'This is not an image';
    }
}

function select_all_posts_info() {
    require 'db_connect.php';
//    $sql = "SELECT * FROM tbl_posts WHERE deletion_status=1";
    $sql = "SELECT p.*, a.author_name, c.category_name, t.tag_name FROM tbl_posts as p, tbl_author as a, tbl_category as c, tbl_tag as t WHERE p.author_id=a.author_id AND p.category_id=c.category_id AND p.tag_id=t.tag_id ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_published_category() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_category WHERE deletion_status=1 AND publication_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_category = mysqli_query($db_connect, $sql);
        return $query_category;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_author() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_author = mysqli_query($db_connect, $sql);
        return $query_author;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_tags() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_tag";
    if (mysqli_query($db_connect, $sql)) {
        $query_tag = mysqli_query($db_connect, $sql);
        return $query_tag;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function update_posts_info($data) {
    require 'db_connect.php';
    $post_title = addslashes($data['post_title']);
    $post_description = addslashes($data['post_description']);
    $sql = "UPDATE tbl_posts SET post_title='$post_title', post_description='$post_description', author_id='$data[author_id]', category_id='$data[category_id]', tag_id='$data[tag_id]', publication_status='$data[publication_status]]' WHERE post_id='$data[post_id]' ";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Post info update successfully";
        header('Location: manage_post.php');
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

function show_posts_info_by_id($post_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_posts WHERE post_id='$post_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function delete_posts_data($id) {
    include 'db_connect.php';
    $sql = "UPDATE tbl_posts SET deletion_status=0 WHERE post_id='$id' ";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Deleted Successfully";
        header('Location: manage_post.php');
        return $message;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

//---------Add Post, View Post------End-----------//
//
//
//
//
//---------------Start ADD, View, Update, Delete AUTHOR-----------//

function save_author_info($data) {
    require 'db_connect.php';


    require 'db_connect.php';

    $directory = 'post_image/';
    $target_file = $directory . $_FILES['post_image']['name'];
    $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
    $file_size = $_FILES['post_image']['size'];
    $check = getimagesize($_FILES['post_image']['tmp_name']);
    if ($check) {
        if (file_exists($target_file)) {
            echo 'This file is already exists. please try new one';
        } else {
            if ($file_size > 1000000) {
                echo 'File is too large. please try new one';
            } else {
                if ($file_type != 'jpeg' && $file_type != 'jpg' && $file_type != 'png') {
                    echo 'File type is not valid. please try new one';
                } else {
                    move_uploaded_file($_FILES['post_image']['tmp_name'], $target_file);
                    $about_author = addslashes($data['about_author']);
                    $fav_quote = addslashes($data['fav_quote']);
                    $home_address = addslashes($data['home_address']);
                    $sql = "INSERT INTO tbl_author (author_name, about_author, fav_quote, author_image, contact_number, author_email, FB, joining_date, designation, home_address) VALUES ('$data[author_name]','$about_author', '$fav_quote','$target_file', '$data[contact_number]', '$data[author_email]', '$data[FB]', '$data[joining_date]', '$data[designation]', '$home_address' )";
                    if (mysqli_query($db_connect, $sql)) {
                        $message = "Author Info Save Successfully";
                        return $message;
                    } else {
                        die('Query problem' . mysqli_error($db_connect));
                    }
                }
            }
        }
    } else {
        echo 'This is not an image';
    }






    if (mysqli_query($db_connect, $sql)) {
        $message = "Author Info Save Successfully";
        return $message;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_author_info() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function update_author_info($data) {
    include 'db_connect.php';

    $sql = "UPDATE tbl_author SET author_name='$data[author_name]',contact_number='$data[contact_number]', author_email='$data[author_email]]', joining_date='$data[joining_date]]', home_address='$data[home_address]]'  WHERE author_id='$data[author_id]'";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Author info updated successfully";
        header('Location: manage_author.php');
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

function show_author_info($author_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_author WHERE author_id='$author_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function delete_author($id) {
    include 'db_connect.php';
    $sql = "UPDATE tbl_author SET deletion_status=0 WHERE author_id='$id' ";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Deleted Successfully";
        header('Location: manage_author.php');
        return $message;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

////---------------End ADD, View, Update, Delete AUTHOR-----------//
////---------------Start ADD, View, Update, Delete Tag-----------//


function save_tag_info($data) {
    require 'db_connect.php';

    $sql = "INSERT INTO tbl_tag (tag_name, short_description) VALUES ('$data[tag_name]', '$data[short_description]' )";
    if (mysqli_query($db_connect, $sql)) {
        $message = "Tag Info Save Successfully";
        return $message;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function select_all_tag_info() {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_tag WHERE deletion_status=1";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function update_tag_info($data) {
    include 'db_connect.php';

    $sql = "UPDATE tbl_tag SET tag_name='$data[tag_name]',short_description='$data[short_description]' WHERE tag_id='$data[tag_id]'";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Tag info updated successfully";
        header('Location: manage_tag.php');
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

function show_tag_info($tag_id) {
    require 'db_connect.php';
    $sql = "SELECT * FROM tbl_tag WHERE tag_id='$tag_id' ";
    if (mysqli_query($db_connect, $sql)) {
        $query_result = mysqli_query($db_connect, $sql);
        return $query_result;
    } else {
        die('Query problem' . mysqli_error($db_connect));
    }
}

function delete_tag($id) {
    include 'db_connect.php';
    $sql = "UPDATE tbl_tag SET deletion_status=0 WHERE tag_id='$id' ";
    if (mysqli_query($db_connect, $sql)) {
        $_SESSION['message'] = "Deleted Successfully";
        header('Location: manage_tag.php');
        return $message;
    } else {
        die('Query Problem' . mysqli_error($db_connect));
    }
}

////---------------End ADD, View, Update, Delete Tag-----------//
//
//
//admin logout


function admin_logout() {
    unset($_SESSION['admin_id']);
    unset($_SESSION['admin_name']);
    header('Location: index.php');
}
