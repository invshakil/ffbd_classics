<?php
ob_start();

session_start();

$admin_id = $_SESSION['admin_id'];
if ($admin_id == NULL) {
    header('Location: index.php');
}

if (isset($_GET['status'])) {
    require 'functions.php';
    admin_logout();
}

//$resource_id = select_admin_info($_SESSION['admin_id']);
//$admin_info = mysqli_fetch_assoc($resource_id);
//$access_level = $admin_info['access_level'];
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1, maximum-scale=1">
        <title>ADMIN DASHBOARD | FFBD HUB</title>

        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
        <link href="css/londinium-theme.css" rel="stylesheet" type="text/css">
        <link href="css/styles.css" rel="stylesheet" type="text/css">
        <link href="css/icons.css" rel="stylesheet" type="text/css">
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&amp;subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

        <script type="text/javascript" src="js/plugins/charts/sparkline.min.js"></script>

        <script type="text/javascript" src="js/plugins/forms/uniform.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/select2.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/inputmask.js"></script>
        <script type="text/javascript" src="js/plugins/forms/autosize.js"></script>
        <script type="text/javascript" src="js/plugins/forms/inputlimit.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/listbox.js"></script>
        <script type="text/javascript" src="js/plugins/forms/multiselect.js"></script>
        <script type="text/javascript" src="js/plugins/forms/validate.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/tags.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/switch.min.js"></script>

        <script type="text/javascript" src="js/plugins/forms/uploader/plupload.full.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/uploader/plupload.queue.min.js"></script>

        <script type="text/javascript" src="js/plugins/forms/wysihtml5/wysihtml5.min.js"></script>
        <script type="text/javascript" src="js/plugins/forms/wysihtml5/toolbar.js"></script>

        <script type="text/javascript" src="js/plugins/interface/daterangepicker.js"></script>
        <script type="text/javascript" src="js/plugins/interface/fancybox.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/moment.js"></script>
        <script type="text/javascript" src="js/plugins/interface/jgrowl.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/datatables.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/colorpicker.js"></script>
        <script type="text/javascript" src="js/plugins/interface/fullcalendar.min.js"></script>
        <script type="text/javascript" src="js/plugins/interface/timepicker.min.js"></script>

        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript" src="js/application.js"></script>
        <script>
            $(document).ready(function () {
                $("#datepicker").datepicker();
            });
        </script>
        <script>

            function checkDelete() {
//            alert('Test');
                var check = confirm('Are you sure to delete this?');
                if (check) {
                    return true;
                } else {
                    return false;
                }
            }

        </script>

    </head>

    <body>

        <!-- Navbar -->
        <div class="navbar navbar-inverse" role="navigation">
            <div class="navbar-header">
                <a class="navbar-brand" href="../index.php"><h3 style="color: #ffff00; font-family: Arial, Helvetica, sans-serif; ">Visit Website</h3></a>
                <a class="sidebar-toggle"><i class="icon-paragraph-justify2"></i></a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-icons">
                    <span class="sr-only">Toggle navbar</span>
                    <i class="icon-grid3"></i>
                </button>
                <button type="button" class="navbar-toggle offcanvas">
                    <span class="sr-only">Toggle navigation</span>
                    <i class="icon-paragraph-justify2"></i>
                </button>
            </div>

            <ul class="nav navbar-nav navbar-right collapse" id="navbar-icons">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-people"></i>
                        <span class="label label-default">2</span>
                    </a>
                    <div class="popup dropdown-menu dropdown-menu-right">
                        <div class="popup-header">
                            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                            <span>Activity</span>
                            <a href="#" class="pull-right"><i class="icon-paragraph-justify"></i></a>
                        </div>
                        <ul class="activity">
                            <li>
                                <i class="icon-cart-checkout text-success"></i>
                                <div>
                                    <a href="#">Eugene</a> ordered 2 copies of <a href="#">OEM license</a>
                                    <span>14 minutes ago</span>
                                </div>
                            </li>
                            <li>
                                <i class="icon-heart text-danger"></i>
                                <div>
                                    Your <a href="#">latest post</a> was liked by <a href="#">Audrey Mall</a>
                                    <span>35 minutes ago</span>
                                </div>
                            </li>
                            <li>
                                <i class="icon-checkmark3 text-success"></i>
                                <div>
                                    Mail server was updated. See <a href="#">changelog</a>
                                    <span>About 2 hours ago</span>
                                </div>
                            </li>
                            <li>
                                <i class="icon-paragraph-justify2 text-warning"></i>
                                <div>
                                    There are <a href="#">6 new tasks</a> waiting for you. Don't forget!
                                    <span>About 3 hours ago</span>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <i class="icon-paragraph-justify2"></i>
                        <span class="label label-default">6</span>
                    </a>
                    <div class="popup dropdown-menu dropdown-menu-right">
                        <div class="popup-header">
                            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                            <span>Messages</span>
                            <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                        </div>
                        <ul class="popup-messages">
                            <li class="unread">
                                <a href="#">
                                    <img src="http://placehold.it/300" alt="" class="user-face">
                                    <strong>Eugene Kopyov <i class="icon-attachment2"></i></strong>
                                    <span>Aliquam interdum convallis massa...</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="http://placehold.it/300" alt="" class="user-face">
                                    <strong>Jason Goldsmith <i class="icon-attachment2"></i></strong>
                                    <span>Aliquam interdum convallis massa...</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="http://placehold.it/300" alt="" class="user-face">
                                    <strong>Angel Novator</strong>
                                    <span>Aliquam interdum convallis massa...</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="http://placehold.it/300" alt="" class="user-face">
                                    <strong>Monica Bloomberg</strong>
                                    <span>Aliquam interdum convallis massa...</span>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <img src="http://placehold.it/300" alt="" class="user-face">
                                    <strong>Patrick Winsleur</strong>
                                    <span>Aliquam interdum convallis massa...</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="dropdown">
                    <a data-toggle="dropdown" class="dropdown-toggle">
                        <i class="icon-grid"></i>
                    </a>
                    <div class="popup dropdown-menu dropdown-menu-right">
                        <div class="popup-header">
                            <a href="#" class="pull-left"><i class="icon-spinner7"></i></a>
                            <span>Tasks list</span>
                            <a href="#" class="pull-right"><i class="icon-new-tab"></i></a>
                        </div>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th class="text-center">Priority</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><span class="status status-success item-before"></span> <a href="#">Frontpage fixes</a></td>
                                    <td><span class="text-smaller text-semibold">Bugs</span></td>
                                    <td class="text-center"><span class="label label-success">87%</span></td>
                                </tr>
                                <tr>
                                    <td><span class="status status-danger item-before"></span> <a href="#">CSS compilation</a></td>
                                    <td><span class="text-smaller text-semibold">Bugs</span></td>
                                    <td class="text-center"><span class="label label-danger">18%</span></td>
                                </tr>
                                <tr>
                                    <td><span class="status status-info item-before"></span> <a href="#">Responsive layout changes</a></td>
                                    <td><span class="text-smaller text-semibold">Layout</span></td>
                                    <td class="text-center"><span class="label label-info">52%</span></td>
                                </tr>
                                <tr>
                                    <td><span class="status status-success item-before"></span> <a href="#">Add categories filter</a></td>
                                    <td><span class="text-smaller text-semibold">Content</span></td>
                                    <td class="text-center"><span class="label label-success">100%</span></td>
                                </tr>
                                <tr>
                                    <td><span class="status status-success item-before"></span> <a href="#">Media grid padding issue</a></td>
                                    <td><span class="text-smaller text-semibold">Bugs</span></td>
                                    <td class="text-center"><span class="label label-success">100%</span></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </li>

                <li class="user dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown">
                        <img src="http://placehold.it/300">
                        <span><?php echo $_SESSION['admin_name']; ?></span>
                        <i class="caret"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right icons-right">
                        <li><a href="#"><i class="icon-user"></i> Profile</a></li>
                        <li><a href="#"><i class="icon-bubble4"></i> Messages</a></li>
                        <li><a href="#"><i class="icon-cog"></i> Settings</a></li>
                        <li><a href="?status=logout"><i class="icon-exit"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- /navbar -->


        <!-- Page container -->
        <div class="page-container">


            <!-- Sidebar -->
            <div class="sidebar">
                <div class="sidebar-content">

                    <!-- User dropdown -->
                    <div class="user-menu dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="http://placehold.it/300">
                            <div class="user-info">
                                Madison Gartner <span>Web Developer</span>
                            </div>
                        </a>
                        <div class="popup dropdown-menu dropdown-menu-right">
                            <div class="thumbnail">
                                <div class="thumb">
                                    <img src="http://placehold.it/300">
                                    <div class="thumb-options">
                                        <span>
                                            <a href="#" class="btn btn-icon btn-success"><i class="icon-pencil"></i></a>
                                            <a href="#" class="btn btn-icon btn-success"><i class="icon-remove"></i></a>
                                        </span>
                                    </div>
                                </div>

                                <div class="caption text-center">
                                    <h6>Madison Gartner <small>Front end developer</small></h6>
                                </div>
                            </div>

                            <ul class="list-group">
                                <li class="list-group-item"><i class="icon-pencil3 text-muted"></i> My posts <span class="label label-success">289</span></li>
                                <li class="list-group-item"><i class="icon-people text-muted"></i> Users online <span class="label label-danger">892</span></li>
                                <li class="list-group-item"><i class="icon-stats2 text-muted"></i> Reports <span class="label label-primary">92</span></li>
                                <li class="list-group-item"><i class="icon-stack text-muted"></i> Balance <h5 class="pull-right text-danger">$45.389</h5></li>
                            </ul>
                        </div>
                    </div>
                    <!-- /user dropdown -->


                    <!-- Main navigation -->
                    <ul class="navigation">
                        <li class="active"><a href="admin_master.php"><span>Dashboard</span> <i class="icon-screen2"></i></a></li>

                        <?php if ($_SESSION['access_level'] != 2) { ?>
                            <li>
                                <a href="#"><span>Admin Panel</span> <i class="icon-user-plus"></i></a>
                                <ul>
                                    <li><a href="add_admin_user.php">Add Admin</a></li>
                                    <li><a href="manage_admin_user.php">Manage Admin</a></li>
                                </ul>
                            </li>
                        <?php } else { ?>

                        <?php } ?>

                        <li>
                            <a href="#"><span>Category</span> <i class="icon-pagebreak"></i></a>
                            <ul>
                                <li><a href="add_category.php">Create Category</a></li>
                                <li><a href="manage_category.php">Manage Category</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="#"><span>Content Post</span> <i class="icon-copy2"></i></a>
                            <ul>
                                <li><a href="new_post.php">New Post</a></li>
                                <li><a href="manage_post.php">View Posts</a></li>
                            </ul>
                        </li>
                        
                        <?php if ($_SESSION['access_level'] != 2) { ?>
                        <li>
                            <a href="#"><span>Author</span> <i class="icon-bubble-user"></i></a>
                            <ul>
                                <li><a href="add_author.php">Add Author</a></li>
                                <li><a href="manage_author.php">Author List</a></li>
                            </ul>
                        </li>
                        <?php } else { ?>

                        <?php } ?>

                        <li>
                            <a href="#"><span>Tags</span> <i class="icon-tags"></i></a>
                            <ul>
                                <li><a href="add_tag.php">Create Tag</a></li>
                                <li><a href="manage_tag.php">Tag List</a></li>
                            </ul>
                        </li>
                    </ul>
                    <!-- /main navigation -->

                </div>
            </div>
            <!-- /sidebar -->


            <!-- Page content -->
            <div class="page-content">
                <!-- Start of content Mastering -->
                <?php
                if (isset($pages)) {
                    if ($pages == 'add_admin_user') {
                        include './pages/add_admin_user_content.php';
                    } else if ($pages == 'manage_admin_user') {
                        include './pages/manage_admin_user_content.php';
                    } else if ($pages == 'update_admin_user') {
                        include './pages/update_admin_user_content.php';
                    }
                    if ($pages == 'add_category') {
                        include './pages/add_category_content.php';
                    } else if ($pages == 'manage_category') {
                        include './pages/manage_category_content.php';
                    } else if ($pages == 'update_category') {
                        include './pages/update_category_content.php';
                    }

                    if ($pages == 'new_post') {
                        include './pages/new_post_content.php';
                    } else if ($pages == 'manage_post') {
                        include './pages/manage_post_content.php';
                    } else if ($pages == 'update_post') {
                        include './pages/update_post_content.php';
                    }

                    if ($pages == 'add_author') {
                        include './pages/add_author_content.php';
                    } else if ($pages == 'manage_author') {
                        include './pages/manage_author_content.php';
                    } else if ($pages == 'update_author') {
                        include './pages/update_author_content.php';
                    }

                    if ($pages == 'add_tag') {
                        include './pages/add_tag_content.php';
                    } else if ($pages == 'manage_tag') {
                        include './pages/manage_tag_content.php';
                    } else if ($pages == 'update_tag') {
                        include './pages/update_tag_content.php';
                    }
                } else {
                    include './pages/admin_home_content.php';
                }
                ?>
                <!-- End of Mastering -->
            </div>
            <!-- /page content -->



        </div>
        <!-- /page container -->

    </body>
</html>